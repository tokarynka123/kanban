import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {BoardComponent} from './components/board/board.component';
import {TaskDetailsComponent} from './components/task-details/task-details.component';

const routes: Routes = [
  {path: '', component: BoardComponent },
  // {path: 'board', component: BoardComponent},
  {path: 'tasks/:id', component: TaskDetailsComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
