import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCardModule, MatDividerModule, MatGridListModule, MatIconModule, MatListModule} from '@angular/material';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {BoardComponent} from './components/board/board.component';
import {TaskComponent} from './components/task/task.component';
import {ListComponent} from './components/list/list.component';
import {TaskDetailsComponent} from './components/task-details/task-details.component';
import {TaskkeeperService} from './services/taskkeeper.service';
import {StickerService} from './services/sticker.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    BoardComponent,
    TaskComponent,
    ListComponent,
    TaskDetailsComponent

  ],
  imports: [
    BrowserModule, AppRoutingModule, BrowserAnimationsModule,
    MatCardModule, MatDividerModule, MatGridListModule,
    DragDropModule, MatListModule, MatButtonModule, MatIconModule,
    FormsModule, ReactiveFormsModule

  ],
  providers: [TaskkeeperService, StickerService],
  bootstrap: [AppComponent]
})

export class AppModule {
}
