import {Component, OnInit} from '@angular/core';
import {StickerService} from '../../services/sticker.service';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit {
  // taskKeeper: TaskkeeperService;
  lists: any;

  constructor(private stickerService: StickerService) {
  }

  getStickers(): void {
    this.stickerService.getTasks().subscribe(stickers => this.lists = stickers);
  }

  ngOnInit() {
    // this.setMockData();
    // this.taskKeeper = new TaskkeeperService();
    this.getStickers();
  }

}
