import {Component, Input, OnInit} from '@angular/core';
import {ListSchema} from '../../models/listschema';
import {TaskkeeperService} from '../../services/taskkeeper.service';
import {StickerService} from '../../services/sticker.service';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  @Input() list: ListSchema;
  displayAddCard = false;
  // here is some test change

  constructor(private stickerService: StickerService, private taskKeeper: TaskkeeperService) {
  }

  allowDrop($event) {
    $event.preventDefault();
  }

  toggleDisplayAddCard() {
    this.displayAddCard = !this.displayAddCard;
  }

  ngOnInit(): void {
    this.list.cards = this.taskKeeper.getCardsByStatus(this.list.id);
  }

  drop($event) {
    $event.preventDefault();
    const data = $event.dataTransfer.getData('text');
    StickerService.saveStatus(data, this.list.name);

    // console.log(this.list.name + '  ' + data);

    let target = $event.target;
    const targetClassName = target.className;

    while (target.className !== 'list') {
      target = target.parentNode;
    }
    target = target.querySelector('.cards');

    if (targetClassName === 'card') {
      $event.target.parentNode.insertBefore(document.getElementById(data), $event.target);
    } else if (targetClassName === 'list__title') {
      if (target.children.length) {
        target.insertBefore(document.getElementById(data), target.children[0]);
      } else {
        target.appendChild(document.getElementById(data));
      }
    } else {
      target.appendChild(document.getElementById(data));
    }
  }

  // onEnter(value: string) {
  //   // const card =  this.taskKeeper.newCard(value);
  //   // this.list.cards.push(card);
  // }

  addTask(formVal) {
    console.log(formVal);
    const card = this.taskKeeper.newCard(formVal.description, formVal.priority);
    this.list.cards.push(card);
    this.toggleDisplayAddCard();
  }
}
