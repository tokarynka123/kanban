import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TaskkeeperService} from '../../services/taskkeeper.service';

@Component({
  selector: 'app-task-details',
  templateUrl: './task-details.component.html',
  styleUrls: ['./task-details.component.css']
})

export class TaskDetailsComponent implements OnInit {
  task: any;

  constructor(private taskkeeper: TaskkeeperService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.task = this.taskkeeper.getCard(this.route.snapshot.params['id']);
  }
}
