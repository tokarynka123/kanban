import { Component, Input, OnInit } from '@angular/core';
import { CardSchema } from '../../models/cardschema';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {
  @Input() card: CardSchema;
  constructor() { }
  visible: boolean;

  ngOnInit() {
  }

  toggleContent() {
    this.visible = !this.visible;
  }
  dragStart(ev) {
    ev.dataTransfer.setData('text', ev.target.id);
  }

  // getTitleClass() {
  //   if (this.card.id === 1 ) {
  //     return ('font-size.px:24');
  //   }
  //   return ;
  //
  // }
}
