export class CardSchema {
  public id: number;
  description: string;
  priority: number;
  statusId: number;
}
