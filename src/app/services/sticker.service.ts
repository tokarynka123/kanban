import {Injectable} from '@angular/core';
// import {Task} from './task';
import {ListSchema} from '../models/listschema';
import {Subject} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class StickerService {

  constructor() {
  }

  static saveStatus(taskId: string, status: string): void {
    console.log('Task: ' + taskId + ' has changed its status to : ' + status);
  }

  getTasks() {
    const lists: ListSchema[] = [
      {
        id: 0,
        name: 'To Do:',
        cards: []
      },
      {
        id: 1,
        name: 'In progress:',
        cards: []
      },
      {
        id: 2,
        name: 'Done:',
        cards: []
      }
    ];
    const subject = new Subject();
    setTimeout(() => {
      subject.next(lists);
      subject.complete();
    }, 200);
    return subject;
  }

}
