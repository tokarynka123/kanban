import {CardSchema} from '../models/cardschema';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TaskkeeperService {
  cards: CardSchema[] = [
    {
      id: 0,
      priority: 1,
      description: 'desc 0',
      statusId: 0
    },
    {
      id: 1,
      priority: 2,
      description: 'desc 1',
      statusId: 0
    },
    {
      id: 2,
      priority: 3,
      description: 'desc 2',
      statusId: 1
    }];
  lastid = 2;

  _addCard(card: CardSchema) {
    card.id = ++this.lastid;
    this.cards.push(card);
    console.log('New task created: ' + this.lastid);
    return (card);
  }

  getCardsByStatus(sId): CardSchema[] {
    return this.cards.filter(x => x.statusId.valueOf() === sId);
  }


  getCard(cardId: number) {
    return this.cards[cardId];
  }

  newCard(description: string, priority: string): CardSchema {
    const card = new CardSchema();
    card.description = description;
    card.priority = +priority;
    return (this._addCard(card));
  }

}
